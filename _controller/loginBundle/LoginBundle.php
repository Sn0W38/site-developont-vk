<?php

$error = array();
$data = NULL;
$password = NULL;
$pseudo = NULL;

if (isset($_POST['submit']))
{

    if (!empty($_POST['pseudo']))
    {
        if (isset($_POST['pseudo']))
        {
            $pseudo = htmlspecialchars($_POST['pseudo']);
        }
        else
        {
            $error['pseudo'] = 'Veuillez saisir votre pseudo';
        }
    }
    else
    {
        $error['pseudo'] = 'Veuillez saisir votre pseudo';
    }

    if (!empty($_POST['password']))
    {
        if (isset($_POST['password']))
        {
            $password = htmlspecialchars($_POST['password']);
        }
        else
        {
            $error['password'] = 'Veuillez saisir votre mot de passe';
        }
    }
    else
    {
        $error['password'] = 'Veuillez saisir votre mot de passe';
    }

    if ($pseudo)
    {
        $data = verif($pseudo);
    }
    else
    {
        $error['pseudo'] = 'Veuillez saisir votre pseudo';
    }

    if ($data['pseudo'] == $pseudo)
    {
        if (password_verify($password, $data['password']))
        {
            $_SESSION['pseudo'] = $data['pseudo'];
            $_SESSION['id'] = $data['id'];
            header('Location:_assets/views/extranet/member.php');
        }
        else
        {
            $_SESSION['pseudo'] = NULL;
            $error['password']= 'Mot de passe incorrect';
        }
    }
    else
    {
        $error['data'] = 'L\'utilisateur n\'existe pas';
    }
}

/**
*   @author     Kevin
*   @date       19/02/2018
*   @version    1.0
*
*   @function verif
*   Verifie la présence de $param dans la bdd,
*       return toute les valeurs enregistrés dans la bdd au $param données
*
*   @param      string $param | pseudo, '$_POST'
*   @return     false or data
**/

function verif($param)
{
    require 'php/bdd.php';
    $data = NULL;
    $req = $connection->prepare($sql['selectAllWithPseudo']) or die(print_r($bdd->errorInfo()));
    $reponse = $req->execute(array('pseudo' => $param));

    if ($reponse)
    {
        $data = $req->fetch();
    }
    unset($req);

    return $data;
}
