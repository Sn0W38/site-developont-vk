<?php
/**
*   @author     Kevin
*   @date       19/02/2018
*   @version    1.0
*
*   @function listMember
*   affiche tout les données de chaque utilisateur inscrit
*
**/

function listeMember()
{
    require '../../../php/bdd.php';

    $req = $connection->prepare($sql['selectAll']);
    $req->execute();
    $data = $req->fetchAll(PDO::FETCH_OBJ);

    if ($req)
    {
        foreach ($data as $value)
        {
            if ($value->password)
            {
                $password = '<button type="button" class="btn btn-success"><i class="fas fa-2x fa-check-square"></i></button>';
            }
            echo "  <tr>
                        <td> ".$value->pseudo." </td>
                        <td> ".$password."</td>
                        <td><button type='button' data-toggle='modal'  class='btn btn-primary' data-target='#exampleModalCenter".$value->id."'><i class='fab fa-whmcs fa-2x text-light'></i></button></td>
                        <td><a class='btn btn-danger' onclick=\"return confirm('Êtes vous sur de vouloir supprimer ".$value->pseudo." ?')\" href='../../../php/fonctions/delete.php?id=".$value->id."'><i class='fas fa-2x fa-trash-alt text-light'></i></a></td>
                    </tr>";
            echo '  <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter'.$value->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <form method="POST" action="">
                                        <input type="hidden" name="id" value="'.$value->id.'">
                                          <div class="form-group">
                                              <label for="pseudo">Votre pseudo : '.$value->pseudo.'</label><br>
                                          </div>
                                          <div class="form-group">
                                              <label for="passwordEdit">Mot de passe :</label>
                                              <input type="password" name="passwordEdit" class="form-control">
                                          </div>
                                          <div class="form-group">
                                              <label for="passwordEditCheck">Confirmation mot de passe :</label>
                                              <input type="password" name="passwordEditCheck" class="form-control">
                                          </div>
                                          <div class="form-group">
                                              <button type="submit" class="btn btn-info">Valider</button>
                                          </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>';
        }
    }
    unset($req);
}
/*----------------------------------------------------------------------------*/
/**
*   @author     Kevin
*   @date       19/02/2018
*   @version    1.0
*
*   @function register
*   Enregistre un nouvelle utilisateur dans la bdd
*
*   @param  string $data
*
**/

function register($data)
{
    require '../../../php/bdd.php';
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $req = $connection->prepare($sql['create']);
    $req -> execute(array(  'pseudo' => $data['pseudo'],
                            'password' => $data["password"],));
    unset($req);
}
/*----------------------------------------------------------------------------*/
/**
*   @author     Kevin
*   @date       19/02/2018
*   @version    1.0
*
*   @function inscription new member
*
*   @param      string $post
*   @return     $errorMsg
**/

function inscription($post)
{
    require '../../../php/bdd.php';
    $checkPseudo = false;
    $checkPassword = false;

    $errorRegister = array();

    if (isset($post['pseudo']))
    {
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $req = $connection->prepare($sql['selectOne']) or die(print_r($db->errorInfo()));
        $checkPseudoUsed = $req->execute(array('pseudo' => $post['pseudo']));
        $checkPseudoUsed = $req->fetch();
        $req->closeCursor();
    }

    $data = [   'pseudo'    => '',
                'password'  => ''
            ];

    if (isset($post['pseudo']))
    {
        if (!empty($post['pseudo']))
        {
            if ($checkPseudoUsed)
            {
                $errorRegister['pseudo'] = 'Pseudo deja utilisé';
            }
            else
            {
                $data['pseudo'] = htmlspecialchars($post['pseudo']);
                $checkPseudo = true;
            }
        }
        else
        {
            $errorRegister['pseudo'] = 'champ vide';
        }
    }

    if (isset($post['password']))
    {
        if (!empty($post['password']))
        {
            if (preg_match("/([0-9A-Za-z])/",$_POST['password']))
            {
                if ($post["password"] == $post["password_confirm"])
                {
                    $data['password'] = password_hash($post['password'], PASSWORD_BCRYPT);
                    $checkPassword = true;
                }
                else
                {
                    $errorRegister['password'] = "Vous n'avez pas rentré le même mot de passe.";
                }
            }
            else
            {
                $errorRegister['password'] = 'caractére interdit (mot de passe)';
            }

        }
        else
        {
            $errorRegister['pseudo'] = 'champ password vide';
        }
    }
    if ($checkPseudo && $checkPassword)
    {
        register($data);
        header("Location: member.php");
    }
        return $errorRegister;
}
