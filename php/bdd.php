<?php
    /**
    * Type : Constant.
    * Content : Contains all of value for connecting to the data base.
    **/
    $db = 'mysql:host=localhost;dbname=developonteur'; // Name of the data base.
    $name = 'wordpress'; // User Login for the DB.
    $password = '112246aB?'; // User password for the DB.
    $options = []; // some options.

    try
    {
        $connection = new PDO($db, $name, $password, $options);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        die("Erreur : ".$e->getMessage());
        $db->rollBack();
    };

    /**
    * Type: array
    * Content : All SQL request
    * 'create' = sql request for create a new user from intra_user with pseudo and password
    * 'selectAll' = sql request for select all  from intra_user
    * 'selectOne' = sql request for select an pseudo from intra_user with a pseudo
    * 'selectAllWithPseudo' = sql request for select all from intra_user with a pseudo
    * 'delete' = sql request for Delete a user from intra_user with ID
    * 'update' = sql request for Update a user from intra_user with ID
    **/
    $sql = array(
        'create' => 'INSERT INTO intra_user(pseudo, password) VALUES(:pseudo, :password)',
        'selectAll' => 'SELECT * FROM intra_user',
        'selectOne' => 'SELECT pseudo FROM intra_user WHERE pseudo = :pseudo',
        'selectAllWithPseudo' => 'SELECT * FROM intra_user WHERE pseudo = :pseudo',
        'delete' => 'DELETE FROM intra_user WHERE id=:id',
        'selectOneId' => 'SELECT * FROM intra_user WHERE id=:id',
        'update' => 'UPDATE intra_user SET password=:password WHERE id=:id',

    );
