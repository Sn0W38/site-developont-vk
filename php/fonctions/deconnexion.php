<?php
/*
*   deconnexion : destruction de la $_SESSION
*/
  session_start();
  session_destroy();
  header('location: ../../index.php');
  exit;
?>
