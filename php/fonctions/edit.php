<?php

    $error = array();

    $id = '';
    if (isset($_POST['id']))
    {
        $id = $_POST['id'];
    }

    $req = $connection->prepare($sql['selectOneId']);
    $req->execute([':id' => $id ]);
    $data = $req->fetch(PDO::FETCH_OBJ);

    $checkPassword = false;

    if (isset($_POST['passwordEdit']))
    {
        if (!empty($_POST['passwordEdit']))
        {
            if (preg_match("/([0-9A-Za-z])/",$_POST['passwordEdit']))
            {
                if ($_POST['passwordEditCheck'] == $_POST['passwordEdit'])
                {
                    $mdp = htmlspecialchars($_POST['passwordEdit']);
                    $passwordEdit = password_hash($mdp, PASSWORD_BCRYPT);
                    $checkPassword = true;
                }
                else
                {
                    $error['passwordSet'] = 'Les mots de passe ne sont pas identique';
                }
            }
        }
        else
        {
            $error['passwordSet'] = 'Veuillez saisir votre mot de passe';
        }
    }

    if ($checkPassword)
    {
        if ($_SESSION['pseudo'] == $data->pseudo or $_SESSION['pseudo'] == 'admin')
        {
            $req = $connection->prepare($sql['update']);
            if ($req->execute([ ':password' => $passwordEdit,
            ':id' => $id]))
            {
                $error['formulaireSend'] = '<p class="text-success m-0 p-0">Mot de passe modifier avec success</p>';
            }
        }
        else
        {
            $error['set'] = 'Vous n\'avez pas le droit de modifier ce profil';
        }
    }
