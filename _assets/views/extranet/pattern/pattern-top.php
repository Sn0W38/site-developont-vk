<?php
require_once('../../../php/logged.php');
require_once('../../../php/bdd.php');

require '../../../php/fonctions/edit.php';
require '../../../_controller/extranetBundle/extranetBundle.php';


        if (isset($_POST['pseudo']))
        {
            $errorRegister = inscription($_POST);
        }
?>
<!DOCTYPE html>
<html lang='fr'>
    <head>
        <?php include 'head.html'; ?>
        <link rel="stylesheet" type="text/css" href="<?php echo $_SERVER['DOCUMENT_ROOT'];?> site-developont-vk/_assets/css/style-extranet.css">
        <link rel="stylesheet" type="text/css" href="../../css/style-extranet.css">
    </head>

    <body class="">

        <div class="row d-flex col-12 p-0 m-0">

            <div class="pattern-color col-sm-2 p-0 m-0">
                <nav class="nav flex-column">
                    <li class="nav-item"><a class="nav-link<?php if($page == 'index.php') {echo ' active';} ?>" href="#"><i class="fas fa-home"></i> Accueil</a></li>
                    <li class="nav-item"><a class="nav-link<?php if($page == '#') {echo ' active';} ?>" href="#"><i class="fas fa-user"></i> Recrutement</a></li>
                    <li class="nav-item"><a class="nav-link<?php if($page == 'member.php') {echo ' active';} ?>" href="member.php"><i class="fas fa-tachometer-alt"></i> Gestion membres</a></li>
                </nav>
            </div>

            <div class="row col-sm-10 p-0 m-0 text-center justify-content-center">
                <div class="container col-12 m-0 p-0">
                    <div class="color1 col-12 mb-4 p-0">
                        <ul class="nav d-flex align-items-center">
                            <li class="nav-item p-2"><a href="extranet.php"><img src='../../images/logo/developont.png'></a></li>
                            <li class="nav-item p-2">Bienvenue sur le portail <?php echo $_SESSION['pseudo']; ?></li>
                            <li class="nav-item ml-auto"><button onClick='location.href="../../../php/fonctions/deconnexion.php"' class="btn rounded pr-2 text-light">Deconnexion <i class="fas fa-times-circle"></i></button></li>
                        </ul>
                    </div>
                </div>
