<?php $page = 'member.php' ?>
<?php require 'pattern/pattern-top.php'; ?>
    <div class="col-sm-10">
        <div class="row col justify-content-center">
            <section class="col-12 mb-2 color1 rounded container">
                <div class="row p-3 pl-4 mb-4 head justify-content-between align-items-center">
                    <h2 class="">Ajouter membres</h2>
                    <i class="fas fa-2x fa-angle-down"></i>
                </div>
                <form method="post" action="" class="d-flex justify-content-center">
                    <div class="text-center mb-4">
                        <div class="col mb-4 form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fab fa-studiovinari"></i></div>
                                </div>
                                <input type="text" name="pseudo" placeholder="pseudo" class="p-2">
                            </div>
                        </div>
                        <div class="col mb-4 form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
                                </div>
                                <input type="password" name="password" placeholder="mot de passe" class="p-2">
                            </div>
                        </div>
                        <div class="col mb-4 form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
                                </div>
                                <input type="password" name="password_confirm" placeholder="confirmation mot de passe" class="p-2">
                            </div>
                        </div>
                        <div class="text-right">
                            <input type="submit" name="valider" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </section>

            <section class="col-12 m-4 color1 rounded">
                <div class="row p-3 pl-4 mb-4 head justify-content-between align-items-center">
                    <h2 class="">Liste des membres</h2>
                    <i class="fas fa-2x fa-angle-down"></i>
                </div>


                <table class='table table-striped mb-4'>
                        <theads>
                            <tr class="table-dark text-dark">
                                <th scope='col'>Pseudo</th>
                                <th scope='col'>Mot de passe</th>
                                <th scope='col'>Modification</th>
                                <th scope='col'>Suppression</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php listeMember();?>
                        </tbody>
                    </table>

            </section>

            <section class="col-12 mb-2 color1 rounded container">
                <div class="row p-3 pl-4 mb-4 head justify-content-between align-items-center">
                    <h2 class="">Erreurs</h2>
                    <i class="fas fa-2x fa-angle-down"></i>
                </div>
                <div class="justify-content-center container">
                    <div class="row">
                        <div class="col">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="table-dark">
                                        <th scope="col" class="text-dark">Partie Ajout membres</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($errorRegister))
                                    {
                                        foreach($errorRegister as $value => $data)
                                        {
                                            echo "<tr><td class='text-danger'>".$data."</td></tr>";
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="table-dark">
                                        <th scope="col" class="text-dark">Partie Modification</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(isset($error))
                                    {
                                        foreach($error as $value => $data)
                                        {
                                            echo "<tr><td class='text-danger'>".$data."</td></tr>";
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
<?php require 'pattern/pattern-bottom.php'; ?>
