<?php
	session_start();
	include("_controller/loginBundle/LoginBundle.php");
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<?php include '_assets/views/head.html'?>
        <link rel="stylesheet" type="text/css" href="_assets/css/style-connexion.css">
	</head>

	<body class="container rounded">

		<main class="row bg-white rounded">
			<div class="col p-4 border-right">
				<img src="_assets/images/logo/logo.png" alt="develo'pont">
			</div>
			<div class="col align-self-center">
				<p>Login: demo Mdp: demo</p>
				<h1 class="m-4 border-bottom">Intra'Develo</h1>
				<form class="container mt-5" method="post" action="login.php">

					<div class="col my-1 mb-4 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fab fa-studiovinari"></i></div>
							</div>
							<input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="Pseudo">
						</div>
					</div>

					<div class="col mb-4 form-group">
						<div class="input-group">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
							</div>
							<input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe">
						</div>
					</div>

					<div class="row ml-2 mr-2">
						<div class="col-8">
							<?php
								if(isset($error)) {
									echo "<ul>";
									foreach($error as $value => $data)
									{
									 echo "<li class='text-danger'>".$data."</li>";
									}
									echo "</ul>";
								}
							?>
						</div>
						<div class="text-right col-4">
							<input type="submit" name='submit' class="btn btn-primary" value='connexion'>
						</div>
					</div>
				</form>
			</div>
		</main>
    </body>

</html>
