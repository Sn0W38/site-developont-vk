<!DOCTYPE html>
<html lang="fr">

	<head>
		<?php include '_assets/views/head.html'?>
	</head>

	<body class="container">

		<nav id="accueil" class="navbar navbar-expand-lg navbar-light bg-light">
        <?php include '_assets/views/navibar.html' ?>
		</nav>

		<main>
			<?php include '_assets/views/carousel.html'; ?>

			<section id="quisommesnous" class="container first-party borderimg-t borderimg-b">
				<h1><i class="fas fa-terminal"></i> Qui sommes nous ?</h1>
				<div class="container first-party-article">
					<p>Develo'pont est une formation professionnel en développement.<br>
						Cette formation est totalement gratuite, sans obligation de diplome, intensive de 7 mois.<br>
						Develo'pont fait partie du reseau de <a href="https://simplon.co/" target="_blank">simplon.co</a><br>
						Cette formation à pour but :
					</p>

					<ul class="text-right">
						<li>D'apprendre à créer des sites Internet, des applications web</li>
						<li>De répondre au besoin du marché du travail</li>
						<li>D'avoir un apprentissage basé sur la pratique, par projets et moins scolaire.</li>
						<li>De former gratuitement de futur développeur.</li>
					</ul>
				</div>

				<div class="container">
					<?php include '_assets/views/module-tableau.html'; ?>
				</div>
				<div class="text-right mt-3">
					<a href="#" class="btn btn-primary">En savoir plus <i class="fas fa-arrow-circle-right"></i></a>
				</div>
			</section>

			<div class="row mt-5 mb-5 ml-2 mr-2">
				<div class="col-sm-6">
				   	<div class="card">
			      		<div class="card-body">
			        		<h5 class="card-title">Je veux candidater</h5>
			        		<p class="card-text">Devenez apprenants de Develo'Pont.</p>
							<button data-toggle="modal" data-target="#ModalForm1" class="btn btn-primary">En savoir plus</button>
				      	</div>
				    </div>
				  </div>

				  <div class="col-sm-6">
				   	<div class="card">
				     	<div class="card-body">
				        	<h5 class="card-title">Je veux devenir partenaires</h5>
				        	<p class="card-text">Devenez partenaires de Develo'Pont.</p>
				        	<button data-toggle="modal" data-target="#ModalForm2" class="btn btn-primary">En savoir plus</button>
				     	</div>
				    </div>
				  </div>
			</div>

			<section class="recrutement borderimg-t">
				<h1 class="text-uppercase font-weight-bold align-middle text-center">Recrutez un Dévelo'pont</h1>
			</section>

			<section id="simplon" class="container borderimg-t borderimg-b">
				<h1><i class="fas fa-code-branch"></i> Simplon.co</h1>

				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 d-flex justify-content-center p-2">
							<img class="img-fluid" src="_assets/images/logo/simplon.png" alt="simplon.co">
						</div>

						<div class="col-sm-8 col-md-8 col-lg-6 col-xl-6 d-flex align-items-center p-2">
							<p class="p-4 text-justify">Simplon est un réseau de fabriques numériques et inclusives en France et à l’étranger. Simplon a formé plus de 1500 apprenants aux métiers du numérique depuis 2013.
								C'est une entreprise sociale et solidaire qui souhaite faire du numérique un véritable levier d’inclusion et révéler des talents parmi des publics peu représentés dans le secteur.
							</p>
						</div>
					</div>
				</div>

				<div class="text-right">
					<a href="https://simplon.co/" class="btn btn-primary">En savoir plus <i class="fas fa-arrow-circle-right"></i></a>
				</div>
			</section>

			<!--Partie map-->
	        <div class="map-area rounded text-center">
	            <div id="map"></div>
	        </div>
	        <!--Partie map-->

			<section class="container borderimg-t" id="partenaires">
				<h1><i class="far fa-handshake"></i> Nos partenaires</h1>
				<div class="container img-fluid party-partenaires">
					<div class="row justify-content-center">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/bpifrance.png" alt="">
						<img class="p-2 m-2 fluid-img"  src="_assets/images/logo/partenaires/EDF.png" alt="">
						<img class="p-2 m-2 fluid-img"  src="_assets/images/logo/partenaires/emploi.png" alt="">
						<img class="p-2 m-2 fluid-img"  src="_assets/images/logo/partenaires/EPN.png" alt="">
						<img class="p-2 m-2 fluid-img"  src="_assets/images/logo/partenaires/Fond_paritaire.png" alt="">
						<img class="p-2 m-2 fluid-img"  src="_assets/images/logo/partenaires/formation.png" alt="">
						<img class="p-2 m-2 fluid-img"  src="_assets/images/logo/partenaires/FSE.png" alt="">
					</div>
					<div class="row justify-content-center">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/france_active.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/Isere.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/leader.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/legrand.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/massif.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/OPCALIA.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/pont_en_royans.png" alt="">
					</div>
					<div class="row justify-content-center">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/Region.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/rosefse.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/royans.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/sncf.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/StMarcellin.png" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/UE.jpg" alt="">
						<img class="p-2 m-2 fluid-img" src="_assets/images/logo/partenaires/vercors.png" alt="">
					</div>
				</div>
			</section>


		<!-- Modal Section , place here your modal -->

			<!-- Modal 1 -->
			<div class="modal fade" id="ModalForm1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  	<div class="modal-dialog modal-dialog-centered" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<h5 class="modal-title text-center" id="exampleModalLabel">Je veux Candidater</h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body">
			        		<p> Avant de remplir le formulaire, nous vous conseillons de vérifier que vous avez bien effectuer les pré-requis :</p>
							<ul>
								<li>- avoir passer au moins 30 Badges sur <a href="http://codecademy.com/">Code Academy</a></li>
								<li>- être majeur </li>
							</ul>
							<p>Si vous remplissez les conditions, nous vous invitons à remplir le formulaire <a href="https://docs.google.com/forms/d/e/1FAIpQLSeKNYJ9A4VPsIZm9o7GrbDFIYxFFee_Y5Icw0sXwxCUwBjWug/viewform" target="_blank"> ici</a></p>
			      		</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
			      		</div>
			    	</div>
			  	</div>
			</div>

			<!-- Modal 2 -->
			<div class="modal fade" id="ModalForm2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  	<div class="modal-dialog modal-dialog-centered" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<h5 class="modal-title" id="exampleModalLabel">Devenir Partenaire</h5>
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          			<span aria-hidden="true">&times;</span>
			        		</button>
			      		</div>
			      		<div class="modal-body">
			        	Cette Section n'a pas encore était réaliser. Nous vous conseillons de nous contactez par mail ou téléphone
			      		</div>
			      		<div class="modal-footer">
			        		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      		</div>
			    	</div>
			  	</div>
			</div>
		</main>

		<footer class="container borderimg-t borderimg-b p-4" id="contact">
			<div class="sticky-top text-right"><a class="text-primary" href="#accueil">Retour <i class="fas fa-level-up-alt"></i></a></div>
			<div class="d-flex justify-content-around">
				<div class="align-self-center">
					<h2>Develo'pont</h2>
					<p>	Grande rue,<br>
						38680- Pont en royans<br>
						04-76-64-19-96<br>
						developont@gmail.com<br>
					</p>
				</div>

				<div class="align-self-center m-0 p-0">
					<p><a class="" href="#quisommesnous">Dévelo'pont</a></p>
					<p><a class="" href="#">L'équipe</a></p>
					<p><a class="" href="https://simplon.co/">Simplon.co</a></p>
					<p><a class="" href="#">Recruter un develo'pont</a></p>
					<p><a class="" href="#">Média</a></p>

				</div>

				<div class="d-flex flex-column align-self-center">
					<a class="m-2" href="https://fr-fr.facebook.com/developont/" target="_blank"><i class="fab fa-2x fa-facebook-square"></i></a>
					<a class="m-2" href="https://twitter.com/developont" target="_blank"><i class="fab fa-2x fa-twitter-square"></i></a>
					<a class="m-2" href="https://www.linkedin.com/company/develo-pont/" target="_blank"><i class="fab fa-2x fa-linkedin"></i></a>
				</div>
			</div>
			<div class="container border-top border-secondary realis">
				<div class="mt-3 d-flex justify-content-around">
					<p class="m-0 mr-2 p-0"><a href="http://landry-kevin.fr/" target="_blank"><i class="far fa-bookmark"></i> Réalisation Kevin Landry</a></p>
					<p class="m-0 mr-2 p-0"><i class="far fa-copyright"></i> 2018 - Tous droits réservés</p>
					<p class="m-0 mr-2 p-0"><a href="#" target="_blank"><i class="fas fa-gavel"></i> Mention Legal</a></p>
				</div>
			</div>
		</footer>

	</body>
	<?php include '_assets/views/script.html' ?>
</html>
